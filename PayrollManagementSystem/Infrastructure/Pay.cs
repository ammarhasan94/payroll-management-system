﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PayrollManagementSystem.Models;

namespace PayrollManagementSystem.Infrastructure
{
    public class Pay
    {
        static Model1Container db = new Model1Container();
        // working hours minus 1 bcuz lunch is included
        public static double workingHours = (db.Settings.First().ExitTime.Value - db.Settings.First().EntryTime.Value).TotalHours - 1;            
        public static double GetPerHourSalary(int id) {
            Employee employee = db.Employees.Find(id);
            return employee.BasicSalary/(db.Settings.First().WorkingDays.Value *(workingHours));
        }
        
        public static double GetOvertime(Attendance attendance)
        {
            double overtimeHours = 0;
            var hoursWorkedToday = (attendance.CheckOutTime.Value - attendance.CheckInTime.Value).TotalHours - 1;
            if (hoursWorkedToday > workingHours){
                overtimeHours = hoursWorkedToday - workingHours;
            }
            return overtimeHours;
        }
        public static double GetBasicWorkHoursPay(Attendance attendance)
        {
            var dailyWorkHoursPay=0.0;
            //Hours Worked minus to exclude lunch
            var hoursWorkedToday = (attendance.CheckOutTime.Value - attendance.CheckInTime.Value).TotalHours;
            if (hoursWorkedToday > 6)
                hoursWorkedToday--;
            var perHourSalary = Pay.GetPerHourSalary(attendance.Employee_Id);
            //check for overtime
            if (hoursWorkedToday > workingHours)
            {
                // Actual basic working hours pay
                dailyWorkHoursPay = workingHours * perHourSalary;
            }
            else {
                // Actual basic working hours pay
                dailyWorkHoursPay = hoursWorkedToday * perHourSalary;
            }
            // adding one hour pay if lunch time included
            if(attendance.IsLTIncluded)
                dailyWorkHoursPay += perHourSalary;
            return dailyWorkHoursPay;
        }
        public static double GetDailyOvertimePay(Attendance attendance)
        {
            double overtimePay = 0.0;
            double overtimeHours=GetOvertime(attendance);
            overtimePay = overtimeHours * Pay.GetPerHourSalary(attendance.Employee_Id) * db.Employees.Find(attendance.Employee_Id).OvertimeRate.Value;
            return overtimePay;
        }
        public static double GetPayAsOvertime(Attendance attendance)
        {
            var overtimePay = 0.0;
            //Hours Worked minus to exclude lunch
            var hoursWorkedToday = (attendance.CheckOutTime.Value - attendance.CheckInTime.Value).TotalHours - 1;
            overtimePay = hoursWorkedToday * Pay.GetPerHourSalary(attendance.Employee_Id) * db.Employees.Find(attendance.Employee_Id).OvertimeRate.Value;
            // adding one hour pay if lunch time included
            if (attendance.IsLTIncluded)
                overtimePay += Pay.GetPerHourSalary(attendance.Employee_Id);
            return overtimePay;
        }
        public static double GetHourAsOvertime(Attendance attendance)
        {
            double overtimeHours = 0;
            overtimeHours = (attendance.CheckOutTime.Value - attendance.CheckInTime.Value).TotalHours - 1;
            if (attendance.IsLTIncluded)
                overtimeHours++;
            return overtimeHours;
        }
    }
}