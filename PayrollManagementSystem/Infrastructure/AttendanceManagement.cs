﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PayrollManagementSystem.Models;
using System.Web.Mvc;

namespace PayrollManagementSystem.Infrastructure
{
    public class AttendanceManagement
    {
        static Model1Container db = new Model1Container();

        public static void CheckIn(Attendance attendance){
            //if date is not given,which is only the case when we are marking today's attendance
            if (attendance.Date==DateTime.MinValue)
                    attendance.Date = DateTime.Now;
            DateTime todayday = DateTime.Now.Date;
            //Only one instance of attendance of one day
            if (!db.Attendances.Any(x => x.Employee_Id == attendance.Employee_Id && (DbFunctions.TruncateTime(x.Date) == todayday)))
            db.Attendances.Add(attendance);
            db.SaveChanges();
        }
        public static void CheckOutOrEdit(Attendance attendance)
        {
            if (attendance.IsOvertime)
            {
                attendance.OvertimePay = Pay.GetPayAsOvertime(attendance);
            }
            else
            {
                attendance.OvertimePay = Pay.GetDailyOvertimePay(attendance);
                attendance.WorkHoursPay = Pay.GetBasicWorkHoursPay(attendance);
            }
            db.Entry(attendance).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}