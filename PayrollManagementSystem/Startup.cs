﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PayrollManagementSystem.Startup))]
namespace PayrollManagementSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
