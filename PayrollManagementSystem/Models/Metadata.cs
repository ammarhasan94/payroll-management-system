﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PayrollManagementSystem.Models
{
    public class AttendanceMetadata
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM d, yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime Date { get; set; }


        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:d/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> CheckInTime { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:d/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> CheckOutTime { get; set; }

        
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> WorkHoursPay { get; set; }

        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> OvertimePay { get; set; }
    }
    public class EmployeeMetadata
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy }", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DOB { get; set; }
        
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> JoiningDate { get; set; }

        [Required]
        public double BasicSalary { get; set; }
        public bool isBoss { get; set; }

        [Required]
        public Nullable<int> YearlyLeavesAllowed { get; set; }
        [Required]
        public Nullable<int> MonthlyLeavesAllowed { get; set; }
        [Required]
        public Nullable<double> OvertimeRate { get; set; }
    }
    public class SettingMetadata
    {

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:h:mm tt}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EntryTime { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:h:mm tt}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ExitTime { get; set; }
    }


    public class PayrollMetadata{
        
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public double BasicPay { get; set; }

        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public double TotalAllowances { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public double TotalDeductions { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> NetPay { get; set; }
        public System.DateTime PayDate { get; set; }
        public string PayMode { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> TotalOvertimePay { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public double TotalWorkHoursPay { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> WorkHours { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> WorkHoursRate { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> Overtime { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> OvertimeRate { get; set; }
        public Nullable<int> LeavesThisMonth { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> LoanRepayment { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<double> AdvanceRepayment { get; set; }
        [DisplayFormat(DataFormatString = "{0: #.##}")]
        public Nullable<int> Bonus { get; set; }
        public Nullable<int> Absents { get; set; }
    
    }
}