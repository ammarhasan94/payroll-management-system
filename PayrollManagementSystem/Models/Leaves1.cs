//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayrollManagementSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Leaves1
    {
        public int Id { get; set; }
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public string LeaveRemarks { get; set; }
        public int Employee_Id { get; set; }
        public int LeaveType_Id { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual LeaveType LeaveType { get; set; }
    }
}
