//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayrollManagementSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaxType
    {
        public TaxType()
        {
            this.Taxes = new HashSet<Tax>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Rate { get; set; }
    
        public virtual ICollection<Tax> Taxes { get; set; }
    }
}
