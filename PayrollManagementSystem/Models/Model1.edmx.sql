
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/03/2015 20:07:06
-- Generated from EDMX file: C:\Users\hasan_001\documents\visual studio 2013\Projects\PayrollManagementSystem\PayrollManagementSystem\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [PayrollSystem];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AllowancesTypesAllowances]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Allowances] DROP CONSTRAINT [FK_AllowancesTypesAllowances];
GO
IF OBJECT_ID(N'[dbo].[FK_DeductionTypesDeductions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Deductions] DROP CONSTRAINT [FK_DeductionTypesDeductions];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeAdvance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Advances] DROP CONSTRAINT [FK_EmployeeAdvance];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeAllowances]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Allowances] DROP CONSTRAINT [FK_EmployeeAllowances];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeAttendance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attendances] DROP CONSTRAINT [FK_EmployeeAttendance];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeDeductions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Deductions] DROP CONSTRAINT [FK_EmployeeDeductions];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeLeaves]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Leaves1] DROP CONSTRAINT [FK_EmployeeLeaves];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeLoan]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Loans] DROP CONSTRAINT [FK_EmployeeLoan];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeePayroll]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Payrolls] DROP CONSTRAINT [FK_EmployeePayroll];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeTax]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Taxes] DROP CONSTRAINT [FK_EmployeeTax];
GO
IF OBJECT_ID(N'[dbo].[FK_LeaveTypesLeaves]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Leaves1] DROP CONSTRAINT [FK_LeaveTypesLeaves];
GO
IF OBJECT_ID(N'[dbo].[FK_TaxTypeTax]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Taxes] DROP CONSTRAINT [FK_TaxTypeTax];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Advances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Advances];
GO
IF OBJECT_ID(N'[dbo].[Allowances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Allowances];
GO
IF OBJECT_ID(N'[dbo].[AllowancesTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AllowancesTypes];
GO
IF OBJECT_ID(N'[dbo].[Attendances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Attendances];
GO
IF OBJECT_ID(N'[dbo].[Calenders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Calenders];
GO
IF OBJECT_ID(N'[dbo].[Deductions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Deductions];
GO
IF OBJECT_ID(N'[dbo].[DeductionTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeductionTypes];
GO
IF OBJECT_ID(N'[dbo].[Employees]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Employees];
GO
IF OBJECT_ID(N'[dbo].[Leaves1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Leaves1];
GO
IF OBJECT_ID(N'[dbo].[LeaveTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LeaveTypes];
GO
IF OBJECT_ID(N'[dbo].[Loans]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Loans];
GO
IF OBJECT_ID(N'[dbo].[Payrolls]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Payrolls];
GO
IF OBJECT_ID(N'[dbo].[Settings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Settings];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[Taxes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Taxes];
GO
IF OBJECT_ID(N'[dbo].[TaxTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TaxTypes];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Advances'
CREATE TABLE [dbo].[Advances] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DateTaken] datetime  NOT NULL,
    [Amount] float  NOT NULL,
    [Employee_Id] int  NOT NULL
);
GO

-- Creating table 'Allowances'
CREATE TABLE [dbo].[Allowances] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Amount] float  NULL,
    [Employee_Id] int  NOT NULL,
    [AllowancesType_Id] int  NOT NULL
);
GO

-- Creating table 'AllowancesTypes'
CREATE TABLE [dbo].[AllowancesTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'Attendances'
CREATE TABLE [dbo].[Attendances] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [IsAbsent] bit  NULL,
    [IsLTIncluded] bit  NULL,
    [CheckInTime] datetime  NULL,
    [CheckOutTime] datetime  NULL,
    [WorkHoursPay] float  NULL,
    [OvertimeHours] float  NULL,
    [Employee_Id] int  NOT NULL
);
GO

-- Creating table 'Calenders'
CREATE TABLE [dbo].[Calenders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [IsHoliday] bit  NOT NULL,
    [IsStrike] bit  NOT NULL
);
GO

-- Creating table 'Deductions'
CREATE TABLE [dbo].[Deductions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Amount] float  NULL,
    [Employee_Id] int  NOT NULL,
    [DeductionType_Id] int  NOT NULL
);
GO

-- Creating table 'DeductionTypes'
CREATE TABLE [dbo].[DeductionTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [DOB] datetime  NULL,
    [Address] nvarchar(max)  NULL,
    [Gender] nvarchar(max)  NOT NULL,
    [PhoneNo] nvarchar(max)  NULL,
    [MobileNo] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NULL,
    [JoiningDate] datetime  NULL,
    [Designation] nvarchar(max)  NOT NULL,
    [Picture] tinyint  NULL,
    [BasicSalary] float  NOT NULL,
    [isBoss] bit  NOT NULL,
    [CNIC] nvarchar(max)  NULL,
    [YearlyLeavesAllowed] int  NULL,
    [MonthlyLeavesAllowed] int  NULL,
    [OvertimeRate] float  NULL,
    [FatherName] nvarchar(60)  NULL
);
GO

-- Creating table 'Leaves1'
CREATE TABLE [dbo].[Leaves1] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FromDate] datetime  NOT NULL,
    [ToDate] datetime  NOT NULL,
    [LeaveMode] nvarchar(max)  NULL,
    [LeaveRemarks] nvarchar(max)  NULL,
    [Employee_Id] int  NOT NULL,
    [LeaveType_Id] int  NOT NULL
);
GO

-- Creating table 'LeaveTypes'
CREATE TABLE [dbo].[LeaveTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Types] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'Loans'
CREATE TABLE [dbo].[Loans] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TotalLoan] float  NOT NULL,
    [RemainingLoan] float  NOT NULL,
    [Installments] smallint  NOT NULL,
    [MarkUpPercentage] float  NULL,
    [PrincipalAmount] float  NOT NULL,
    [Employee_Id] int  NOT NULL
);
GO

-- Creating table 'Payrolls'
CREATE TABLE [dbo].[Payrolls] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BasicPay] datetime  NOT NULL,
    [TotalAllowances] float  NOT NULL,
    [TotalDeductions] float  NOT NULL,
    [NetPay] float  NULL,
    [PayDate] datetime  NOT NULL,
    [PayMode] nvarchar(max)  NULL,
    [TotalOvertimePay] float  NULL,
    [TotalWorkHoursPay] float  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [Employee_Id] int  NOT NULL
);
GO

-- Creating table 'Settings'
CREATE TABLE [dbo].[Settings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EntryTime] datetime  NOT NULL,
    [ExitTime] datetime  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Taxes'
CREATE TABLE [dbo].[Taxes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Amount] float  NOT NULL,
    [Employee_Id] int  NOT NULL,
    [TaxType_Id] int  NOT NULL
);
GO

-- Creating table 'TaxTypes'
CREATE TABLE [dbo].[TaxTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Rate] float  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Advances'
ALTER TABLE [dbo].[Advances]
ADD CONSTRAINT [PK_Advances]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Allowances'
ALTER TABLE [dbo].[Allowances]
ADD CONSTRAINT [PK_Allowances]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AllowancesTypes'
ALTER TABLE [dbo].[AllowancesTypes]
ADD CONSTRAINT [PK_AllowancesTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [PK_Attendances]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Calenders'
ALTER TABLE [dbo].[Calenders]
ADD CONSTRAINT [PK_Calenders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Deductions'
ALTER TABLE [dbo].[Deductions]
ADD CONSTRAINT [PK_Deductions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DeductionTypes'
ALTER TABLE [dbo].[DeductionTypes]
ADD CONSTRAINT [PK_DeductionTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Leaves1'
ALTER TABLE [dbo].[Leaves1]
ADD CONSTRAINT [PK_Leaves1]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LeaveTypes'
ALTER TABLE [dbo].[LeaveTypes]
ADD CONSTRAINT [PK_LeaveTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Loans'
ALTER TABLE [dbo].[Loans]
ADD CONSTRAINT [PK_Loans]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Payrolls'
ALTER TABLE [dbo].[Payrolls]
ADD CONSTRAINT [PK_Payrolls]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Settings'
ALTER TABLE [dbo].[Settings]
ADD CONSTRAINT [PK_Settings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [Id] in table 'Taxes'
ALTER TABLE [dbo].[Taxes]
ADD CONSTRAINT [PK_Taxes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TaxTypes'
ALTER TABLE [dbo].[TaxTypes]
ADD CONSTRAINT [PK_TaxTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Employee_Id] in table 'Advances'
ALTER TABLE [dbo].[Advances]
ADD CONSTRAINT [FK_EmployeeAdvance]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeAdvance'
CREATE INDEX [IX_FK_EmployeeAdvance]
ON [dbo].[Advances]
    ([Employee_Id]);
GO

-- Creating foreign key on [AllowancesType_Id] in table 'Allowances'
ALTER TABLE [dbo].[Allowances]
ADD CONSTRAINT [FK_AllowancesTypesAllowances]
    FOREIGN KEY ([AllowancesType_Id])
    REFERENCES [dbo].[AllowancesTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AllowancesTypesAllowances'
CREATE INDEX [IX_FK_AllowancesTypesAllowances]
ON [dbo].[Allowances]
    ([AllowancesType_Id]);
GO

-- Creating foreign key on [Employee_Id] in table 'Allowances'
ALTER TABLE [dbo].[Allowances]
ADD CONSTRAINT [FK_EmployeeAllowances]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeAllowances'
CREATE INDEX [IX_FK_EmployeeAllowances]
ON [dbo].[Allowances]
    ([Employee_Id]);
GO

-- Creating foreign key on [Employee_Id] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [FK_EmployeeAttendance]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeAttendance'
CREATE INDEX [IX_FK_EmployeeAttendance]
ON [dbo].[Attendances]
    ([Employee_Id]);
GO

-- Creating foreign key on [DeductionType_Id] in table 'Deductions'
ALTER TABLE [dbo].[Deductions]
ADD CONSTRAINT [FK_DeductionTypesDeductions]
    FOREIGN KEY ([DeductionType_Id])
    REFERENCES [dbo].[DeductionTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DeductionTypesDeductions'
CREATE INDEX [IX_FK_DeductionTypesDeductions]
ON [dbo].[Deductions]
    ([DeductionType_Id]);
GO

-- Creating foreign key on [Employee_Id] in table 'Deductions'
ALTER TABLE [dbo].[Deductions]
ADD CONSTRAINT [FK_EmployeeDeductions]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeDeductions'
CREATE INDEX [IX_FK_EmployeeDeductions]
ON [dbo].[Deductions]
    ([Employee_Id]);
GO

-- Creating foreign key on [Employee_Id] in table 'Leaves1'
ALTER TABLE [dbo].[Leaves1]
ADD CONSTRAINT [FK_EmployeeLeaves]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeLeaves'
CREATE INDEX [IX_FK_EmployeeLeaves]
ON [dbo].[Leaves1]
    ([Employee_Id]);
GO

-- Creating foreign key on [Employee_Id] in table 'Loans'
ALTER TABLE [dbo].[Loans]
ADD CONSTRAINT [FK_EmployeeLoan]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeLoan'
CREATE INDEX [IX_FK_EmployeeLoan]
ON [dbo].[Loans]
    ([Employee_Id]);
GO

-- Creating foreign key on [Employee_Id] in table 'Payrolls'
ALTER TABLE [dbo].[Payrolls]
ADD CONSTRAINT [FK_EmployeePayroll]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeePayroll'
CREATE INDEX [IX_FK_EmployeePayroll]
ON [dbo].[Payrolls]
    ([Employee_Id]);
GO

-- Creating foreign key on [Employee_Id] in table 'Taxes'
ALTER TABLE [dbo].[Taxes]
ADD CONSTRAINT [FK_EmployeeTax]
    FOREIGN KEY ([Employee_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeTax'
CREATE INDEX [IX_FK_EmployeeTax]
ON [dbo].[Taxes]
    ([Employee_Id]);
GO

-- Creating foreign key on [LeaveType_Id] in table 'Leaves1'
ALTER TABLE [dbo].[Leaves1]
ADD CONSTRAINT [FK_LeaveTypesLeaves]
    FOREIGN KEY ([LeaveType_Id])
    REFERENCES [dbo].[LeaveTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LeaveTypesLeaves'
CREATE INDEX [IX_FK_LeaveTypesLeaves]
ON [dbo].[Leaves1]
    ([LeaveType_Id]);
GO

-- Creating foreign key on [TaxType_Id] in table 'Taxes'
ALTER TABLE [dbo].[Taxes]
ADD CONSTRAINT [FK_TaxTypeTax]
    FOREIGN KEY ([TaxType_Id])
    REFERENCES [dbo].[TaxTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TaxTypeTax'
CREATE INDEX [IX_FK_TaxTypeTax]
ON [dbo].[Taxes]
    ([TaxType_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------