﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PayrollManagementSystem.Models
{
    [MetadataType(typeof(AttendanceMetadata))]
    public partial class Attendance
    {

    }

    [MetadataType(typeof(EmployeeMetadata))]
    public partial class Employee
    {
    }

    [MetadataType(typeof(SettingMetadata))]
    public partial class Setting
    {
    }

    [MetadataType(typeof(PayrollMetadata))]
    public partial class Payroll
    {
    }
}