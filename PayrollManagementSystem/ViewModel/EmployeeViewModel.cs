﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Collections;
using System;
namespace PayrollManagementSystem.ViewModels
{
    public class EmployeeListViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Father's Name")]
        public string FatherName { get; set; }

        [Display(Name = "Designation")]
        public string Designation { get; set; }
        public Nullable<System.DateTime> JoiningDate { get; set; }
        
        
    }
    public class EmployeeAttendenceViewModel:EmployeeListViewModel {
        
        [Display(Name = "Is Absent?")]
        public bool IsAbsent { get; set; }
        
        [Display(Name = "CheckIn Time")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CheckInTime { get; set; }
        
        
        [Display(Name = "CheckOut Time")]
        public DateTime CheckOutTime { get; set; }

        [Display(Name = "Include Lunch Time")]
        public bool IsLTIncluded { get; set; }
        //public bool CheckIn { get; set; }
        //public bool CheckOut { get; set; }
    }
    
}