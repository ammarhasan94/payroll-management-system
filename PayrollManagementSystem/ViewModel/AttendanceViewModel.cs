﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PayrollManagementSystem.ViewModels
{
    public class AttendanceListViewModel
    {
        public Nullable<int> Id { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d MMM, yyyy}")]
        public Nullable<System.DateTime> Date { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d MMM, yyyy hh:mm tt}")]
        public Nullable<System.DateTime> CheckInTime { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d MMM, yyyy hh:mm tt}")]
        public Nullable<System.DateTime> CheckOutTime { get; set; }
        public bool IsAbsent { get; set; }
    }
    public class MarkPreviousAttendanceViewModel
    {
        [Required]
        public System.DateTime StartDate { get; set; }
        [Required]
        public System.DateTime EndDate { get; set; }
        public bool IsAbsent { get; set; }
        public bool IsLTIncluded { get; set; }
        public bool IsOvertime { get; set; }
        [Required]
        public Nullable<System.DateTime> CheckInTime { get; set; }
        [Required]
        public Nullable<System.DateTime> CheckOutTime { get; set; }


        public int EmployeeId { get; set; }
    }
}