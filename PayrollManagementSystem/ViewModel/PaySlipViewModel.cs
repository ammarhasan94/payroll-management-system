﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PayrollManagementSystem.ViewModel
{
    public class PaySlipViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Father's Name")]
        public string FatherName { get; set; }

        [Display(Name = "Designation")]
        public string Designation { get; set; }
        public Nullable<System.DateTime> JoiningDate { get; set; }
        public DateTime PayDate { get; set; }
        public DateTime DateTo { get; set; }
        public DateTime DateFrom { get; set; }
        public double RegularHours { get; set; }
        public double RegularHoursRate { get; set; }
        public double RegularHoursAmount { get; set; }
        public double OvertimeHours { get; set; }
        public double OvertimeHoursRate { get; set; }
        public double OvertimeHoursAmount { get; set; }
        public int TotalAbsents { get; set; }
        public int TotalLeaves { get; set; }
        public double NetPay { get; set; }
    }
    public class PayMonth
    {
        //[DisplayName("Pay Month")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:d MMM, yyyy hh:mm tt}")]
        [Required]
        public System.DateTime  Month { get; set; }
    }
}