﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PayrollManagementSystem.Models;

namespace PayrollManagementSystem.Controllers
{
    public class LeaveController : Controller
    {
        private Model1Container db = new Model1Container();
        public JsonResult GetRemainingLeaves(int EmployeeId) {
            return Json(db.Employees.Single(e => e.Id == EmployeeId).YearlyLeavesLeft, JsonRequestBehavior.AllowGet);
        }
        // GET: /Leave/
        public ActionResult Index()
        {
            var leaves1 = db.Leaves1.Include(l => l.Employee).Include(l => l.LeaveType).OrderByDescending(l=>l.FromDate);
            return View(leaves1.ToList());
        }

        // GET: /Leave/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Leaves1 leaves1 = db.Leaves1.Find(id);
            if (leaves1 == null)
            {
                return HttpNotFound();
            }
            return View(leaves1);
        }

        // GET: /Leave/Create
        public ActionResult Create()
        {
            ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name");
            ViewBag.LeaveType_Id = new SelectList(db.LeaveTypes, "Id", "Types");
            return View();
        }

        // POST: /Leave/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,FromDate,ToDate,LeaveRemarks,Employee_Id,LeaveType_Id")] Leaves1 leaves1)
        {
            if (ModelState.IsValid)
            {
                if (db.LeaveTypes.Find(leaves1.LeaveType_Id).IsCutFromLeave)
                {
                    Employee e = db.Employees.Find(leaves1.Employee_Id);
                    int days = leaves1.ToDate.DayOfYear - leaves1.FromDate.DayOfYear + 1;
                    e.YearlyLeavesLeft = e.YearlyLeavesLeft - days;
                    db.Entry(e).State = EntityState.Modified;
                }
                db.Leaves1.Add(leaves1);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", leaves1.Employee_Id);
            ViewBag.LeaveType_Id = new SelectList(db.LeaveTypes, "Id", "Types", leaves1.LeaveType_Id);
            return View(leaves1);
        }
        public ActionResult ForAllEmployees()
        {
            ViewBag.LeaveType_Id = new SelectList(db.LeaveTypes, "Id", "Types");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForAllEmployees([Bind(Include = "Id,FromDate,ToDate,LeaveRemarks,LeaveType_Id")] Leaves1 leaves1)
        {
            if (ModelState.IsValid)
            {
                int days = leaves1.ToDate.DayOfYear - leaves1.FromDate.DayOfYear + 1;
                var AllEmployees = db.Employees.ToList();
                foreach (var employee in AllEmployees)
                {
                    if (db.LeaveTypes.Find(leaves1.LeaveType_Id).IsCutFromLeave)
                    {
                        employee.YearlyLeavesLeft = employee.YearlyLeavesLeft - days;
                        db.Entry(employee).State = EntityState.Modified;
                    }
                    Leaves1 l = new Leaves1{Employee_Id=employee.Id,FromDate=leaves1.FromDate,ToDate=leaves1.ToDate,LeaveType_Id=leaves1.LeaveType_Id,LeaveRemarks=leaves1.LeaveRemarks};
                    db.Leaves1.Add(l);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LeaveType_Id = new SelectList(db.LeaveTypes, "Id", "Types", leaves1.LeaveType_Id);
            return View(leaves1);
        }
        // GET: /Leave/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Leaves1 leaves1 = db.Leaves1.Find(id);
            if (leaves1 == null)
            {
                return HttpNotFound();
            }
            ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", leaves1.Employee_Id);
            ViewBag.LeaveType_Id = new SelectList(db.LeaveTypes, "Id", "Types", leaves1.LeaveType_Id);
            return View(leaves1);
        }

        // POST: /Leave/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,FromDate,ToDate,LeaveRemarks,Employee_Id,LeaveType_Id")] Leaves1 leaves1)
        {
            if (ModelState.IsValid)
            {
                //db.Leaves1.Attach(leaves1);
                var leaveInDb=db.Leaves1.Single(l => l.Id == leaves1.Id);
                int OriginalTypeId = db.Entry(leaveInDb).OriginalValues.GetValue<int>("LeaveType_Id");
                //int CurrentTypeId = db.Leaves1.Find(leaves1.Id).LeaveType_Id;
                db.Entry(leaveInDb).CurrentValues.SetValues(leaves1);
                int CurrentTypeId = db.Entry(leaveInDb).CurrentValues.GetValue<int>("LeaveType_Id");
                bool CurrentIsCutFromLeave = db.LeaveTypes.Find(CurrentTypeId).IsCutFromLeave;
                bool OriginalIsCutFromLeave=db.LeaveTypes.Find(OriginalTypeId).IsCutFromLeave;
                if ( CurrentIsCutFromLeave!=OriginalIsCutFromLeave)
                {
                    Employee e = db.Employees.Find(leaves1.Employee_Id);
                    int days = leaves1.ToDate.DayOfYear - leaves1.FromDate.DayOfYear + 1;
                    if(CurrentIsCutFromLeave)
                        e.YearlyLeavesLeft = e.YearlyLeavesLeft - days;
                    else
                        e.YearlyLeavesLeft = e.YearlyLeavesLeft + days;
                    db.Entry(e).State = EntityState.Modified;
                }
                //db.Entry(leaves1).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", leaves1.Employee_Id);
            ViewBag.LeaveType_Id = new SelectList(db.LeaveTypes, "Id", "Types", leaves1.LeaveType_Id);
            return View(leaves1);
        }

        // GET: /Leave/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Leaves1 leaves1 = db.Leaves1.Find(id);
            if (leaves1 == null)
            {
                return HttpNotFound();
            }
            return View(leaves1);
        }

        // POST: /Leave/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Leaves1 leaves1 = db.Leaves1.Find(id);
            //if (db.LeaveTypes.Find(leaves1.LeaveType_Id).IsCutFromLeave)
            //{
            //    Employee e = db.Employees.Find(leaves1.Employee_Id);
            //    int days = leaves1.ToDate.DayOfYear - leaves1.FromDate.DayOfYear + 1;
            //    //Adding back leaves
            //    e.YearlyLeavesLeft = e.YearlyLeavesLeft +days;
            //    db.Entry(e).State = EntityState.Modified;
            //}
            db.Leaves1.Remove(leaves1);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
