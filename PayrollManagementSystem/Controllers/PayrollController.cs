﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PayrollManagementSystem.Models;
using PayrollManagementSystem.Infrastructure;
using PayrollManagementSystem.ViewModel;

namespace PayrollManagementSystem.Controllers
{
    public class PayrollController : BaseController
    {
        // GET: /Payroll/
        public ActionResult Index()
        {
            var payrolls = db.Payrolls.Include(p => p.Employee);
            return PartialView("Index",payrolls.ToList().OrderByDescending(p=>p.PayDate));
        }

        // GET: /Payroll/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payroll payroll = db.Payrolls.Find(id);
            if (payroll == null)
            {
                return HttpNotFound();
            }
            return View(payroll);
        }

        // GET: /Payroll/Create
        public ActionResult Create()
        {
            //ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name");
            return View(new PayMonth { Month=DateTime.Now.AddMonths(-1)});
        }

        // POST: /Payroll/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Bind(Include="Id,BasicPay,TotalAllowances,TotalDeductions,NetPay,PayDate,PayMode,TotalOvertimePay,TotalWorkHoursPay,StartDate,EndDate,Employee_Id")] Payroll payroll1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PayMonth payMonth)
        {
            if (ModelState.IsValid)
            {
                //var Employee = db.Employees.Single(x => x.Id == payroll.Employee_Id);
                var AllEmployees = db.Employees.ToList();
                foreach (var Employee in AllEmployees)
                {
                    Payroll payroll = new Payroll { PayDate = payMonth.Month };
                    payroll.Employee_Id = Employee.Id;
                    if (db.Payrolls.Where(x => x.Employee_Id == Employee.Id && x.PayDate == payMonth.Month).FirstOrDefault() == null)
                    {
                        payroll.StartDate = new DateTime(payroll.PayDate.Year, payroll.PayDate.Month, 1);
                        payroll.EndDate = payroll.StartDate.AddMonths(1).AddDays(-1);
                        DateTime StartDate = payroll.StartDate.Date;
                        DateTime EndDate = payroll.EndDate.Date;
                        var att = db.Attendances.Where(x => (x.Employee_Id == Employee.Id) && (DbFunctions.TruncateTime(x.Date) >= StartDate) && (DbFunctions.TruncateTime(x.Date) <= EndDate)).Select(x => new { WorkHoursPay = x.WorkHoursPay, OvertimeHoursPay = x.OvertimePay, IsAbsent = x.IsAbsent, CheckOutTime = x.CheckOutTime, CheckInTime = x.CheckInTime, Overtime = x.Overtime }).ToList();

                        payroll.BasicPay = Employee.BasicSalary;
                        payroll.WorkHoursRate = Pay.GetPerHourSalary(payroll.Employee_Id);
                        payroll.OvertimeRate = Pay.GetPerHourSalary(payroll.Employee_Id) * db.Employees.Find(payroll.Employee_Id).OvertimeRate.Value;
                        payroll.TotalWorkHoursPay = att.Sum(x => x.WorkHoursPay).Value;
                        payroll.TotalOvertimePay = att.Sum(x => x.OvertimeHoursPay).Value;
                        payroll.Absents = att.Count(x => x.IsAbsent == true);

                        //double WorkHours = 0;
                        //att.ForEach(x =>
                        //{
                        //    WorkHours += (x.CheckOutTime.Value - x.CheckInTime.Value).TotalHours;
                        //});
                        //payroll.WorkHours = WorkHours;
                        payroll.WorkHours = payroll.TotalWorkHoursPay / payroll.WorkHoursRate;

                        double overtime = 0;
                        att.ForEach(x =>
                        {
                            if (x.Overtime.HasValue)
                                overtime += x.Overtime.Value;
                        });
                        payroll.Overtime = overtime;

                        var loans = db.Loans.Where(x => (x.Employee_Id == Employee.Id) && (x.RemainingLoan > 0)).ToList();
                        double LoanRepayment = 0;
                        if (loans != null)
                            loans.ForEach(l =>
                            {
                                LoanRepayment += l.TotalLoan / l.Installments;
                                l.RemainingLoan -= l.TotalLoan / l.Installments;
                                db.Entry(l).State = EntityState.Modified;
                            });
                        payroll.LoanRepayment = LoanRepayment;
                        var advance = db.Advances.Where(x => (x.Employee_Id == Employee.Id) && (x.DateTaken.Month == payroll.PayDate.Month)).FirstOrDefault();
                        payroll.AdvanceRepayment = (advance == null) ? 0 : advance.Amount;

                        int leavesThisMonth = 0;
                        var leaves = db.Leaves1.Where(x => x.FromDate.Month == payroll.PayDate.Month && x.Employee_Id == payroll.Employee_Id).Select(x => x).ToList();
                        leaves.ForEach(l =>
                        {
                            for (DateTime i = l.FromDate; i.Date <= l.ToDate.Date; i = i.AddDays(1))
                            {
                                if (i.Month == payroll.PayDate.Month)
                                    leavesThisMonth++;
                            }
                        });
                        payroll.LeavesThisMonth = leavesThisMonth;

                        payroll.TotalAllowances = payroll.TotalDeductions = 0;
                        payroll.Bonus = 0;

                        payroll.NetPay = (payroll.TotalAllowances - payroll.TotalDeductions + payroll.TotalOvertimePay + payroll.TotalWorkHoursPay + payroll.Bonus + (payroll.WorkHoursRate * payroll.LeavesThisMonth) - payroll.LoanRepayment - payroll.AdvanceRepayment);
                        db.Payrolls.Add(payroll);
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            //ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", payroll.Employee_Id);
            return View(payMonth);
        }

        // GET: /Payroll/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payroll payroll = db.Payrolls.Find(id);
            if (payroll == null)
            {
                return HttpNotFound();
            }
            ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", payroll.Employee_Id);
            return View(payroll);
        }

        // POST: /Payroll/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,BasicPay,TotalAllowances,TotalDeductions,NetPay,PayDate,PayMode,TotalOvertimePay,TotalWorkHoursPay,StartDate,EndDate,Employee_Id")] Payroll payroll)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payroll).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", payroll.Employee_Id);
            return View(payroll);
        }

        // GET: /Payroll/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payroll payroll = db.Payrolls.Find(id);
            if (payroll == null)
            {
                return HttpNotFound();
            }
            return View(payroll);
        }

        // POST: /Payroll/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Payroll payroll = db.Payrolls.Find(id);
            db.Payrolls.Remove(payroll);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
