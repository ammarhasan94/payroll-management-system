﻿//using System.Web.Script.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PayrollManagementSystem.Models;
using Newtonsoft.Json;
using PayrollManagementSystem.Infrastructure;
using PayrollManagementSystem.ViewModels;

namespace PayrollManagementSystem.Controllers
{
    public class AttendanceController : Controller
    {
        private Model1Container db = new Model1Container();
        public ActionResult ShowAttendenceCard(int id)
        {

            var Employee = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            EmployeeAttendenceViewModel attendance = new EmployeeAttendenceViewModel { Id = id, Name = Employee.Name, FatherName = Employee.FatherName, Designation = Employee.Designation };
            attendance.CheckInTime = DateTime.Now;
            return View("AttendanceCard", attendance);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckIn(EmployeeAttendenceViewModel attendance)
        {
            if (ModelState.IsValid)
            {
                if (attendance.IsAbsent)
                    db.Attendances.Add(new Attendance { Employee_Id = attendance.Id, Date = DateTime.Now, IsAbsent = true });
                else
                    db.Attendances.Add(new Attendance { Employee_Id = attendance.Id, Date = DateTime.Now, CheckInTime = attendance.CheckInTime });
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", attendance.Employee_Id);
            return View(attendance);
        }
        public ActionResult PreviousAttendences(int id)
        {
            var attendances = db.Attendances.Where(a=>a.Employee_Id==id);
            return PartialView("PreviousAttendances", attendances.OrderByDescending(x=>x.Date).ToList());
        }

        public ActionResult AttendanceList()
        {
            DateTime today = DateTime.Now.Date;
            var todayAtttendances = from a in db.Attendances
                                    where DbFunctions.TruncateTime(a.Date) == today
                                    select a;
            var emp_attend = (from e in db.Employees
                              join a in todayAtttendances on e.Id equals a.Employee_Id into ea
                              from todayAttendance in ea.DefaultIfEmpty()
                              select new
                              {
                                  EmployeeId = e.Id,
                                  Name = e.Name,
                                  Designation = e.Designation,
                                  Id = todayAttendance.Id == null ? (int?)null : todayAttendance.Id,
                                  CheckInTime = todayAttendance.CheckInTime.HasValue?todayAttendance.CheckInTime.Value :(DateTime?) null ,
                                  CheckOutTime = todayAttendance.CheckOutTime.HasValue ? todayAttendance.CheckOutTime.Value : (DateTime?)null,
                                  Date = todayAttendance.Date!=null?todayAttendance.Date:(DateTime?)null,
                                  IsAbsent=todayAttendance.IsAbsent==null?false:todayAttendance.IsAbsent,
                              }).ToList();
           
            List<AttendanceListViewModel> l = new List<AttendanceListViewModel>();
            foreach (var item in emp_attend)
            {
                l.Add(new AttendanceListViewModel {Id=item.Id, EmployeeId = item.EmployeeId, Name = item.Name, Designation = item.Designation, Date = item.Date, CheckInTime = item.CheckInTime, CheckOutTime = item.CheckOutTime,IsAbsent=item.IsAbsent });
            }
            return PartialView("AttendanceList", l);
        }

        // GET: /Attendance/
        public ActionResult Index()
        {
            //var attendances = db.Attendances.Include(a => a.Employee);
            //return View(attendances.ToList());
            return View();
        }

        // GET: /Attendance/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = db.Attendances.Find(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            return View(attendance);
        }

        // GET: /Attendance/Create
        public ActionResult MarkAttendance(int EmployeeId)
        {
            ViewBag.Employee_Id = new SelectList(db.Employees, "empId", "Name");
            DateTime today = DateTime.Now.Date;
            var todayAttendance = db.Attendances.Where(x => (x.Employee_Id.Equals(EmployeeId)) && (DbFunctions.TruncateTime(x.Date) == today)).FirstOrDefault();
            Attendance b = new Attendance();
            if (todayAttendance == null)
            {
                b = new Attendance { Date = DateTime.Now, Employee_Id = EmployeeId, Employee = db.Employees.Where(x => x.Id == EmployeeId).SingleOrDefault() };
            }
            else if (todayAttendance.CheckInTime.HasValue || todayAttendance.IsAbsent)
                b = db.Attendances.Find(todayAttendance.Id);
            b.Employee = db.Employees.Find(EmployeeId);
            return View(b);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MarkAttendance([Bind(Include = "Id,Date,IsAbsent,IsLTIncluded,IsOvertime,CheckInTime,CheckOutTime,WorkHoursPay,OvertimePay,Employee_Id")] Attendance attendance)
        {

            if (ModelState.IsValid)
            {
                if (!attendance.IsAbsent && attendance.CheckOutTime.HasValue)
                {
                    //AttendanceManagement.CheckOutOrEdit(attendance);
                    if (attendance.IsOvertime)
                    {
                        attendance.Overtime = Pay.GetHourAsOvertime(attendance);
                        attendance.OvertimePay = Pay.GetPayAsOvertime(attendance);
                    }
                    else
                    {
                        attendance.Overtime = Pay.GetOvertime(attendance);
                        attendance.OvertimePay = Pay.GetDailyOvertimePay(attendance);
                        attendance.WorkHoursPay = Pay.GetBasicWorkHoursPay(attendance);
                    }
                    //if date is not given
                    if (attendance.Date == DateTime.MinValue)
                        attendance.Date = DateTime.Now;
                    db.Entry(attendance).State = EntityState.Modified;
                }
                else
                {
                    //AttendanceManagement.CheckIn(attendance);
                    //if date is not given,which is only the case when we are marking today's attendance
                    if (attendance.Date == DateTime.MinValue)
                        attendance.Date = DateTime.Now;
                    DateTime todayday = DateTime.Now.Date;
                    //Only one instance of attendance of one day
                    if (!db.Attendances.Any(x => x.Employee_Id == attendance.Employee_Id && (DbFunctions.TruncateTime(x.Date) == todayday)))
                        db.Attendances.Add(attendance);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Employee_Id = new SelectList(db.Employees, "empId", "Name", attendance.Employee_Id);
            return View(attendance);
        }
        public ActionResult MarkPreviousAttendance(int EmployeeId)
        {
            MarkPreviousAttendanceViewModel vm = new MarkPreviousAttendanceViewModel { EmployeeId = EmployeeId, StartDate = DateTime.Now, EndDate = DateTime.Now, CheckInTime = db.Settings.First().EntryTime.Value, CheckOutTime = db.Settings.First().ExitTime.Value };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MarkPreviousAttendance(MarkPreviousAttendanceViewModel vm) {
            for (DateTime i = vm.StartDate; i.Date <=vm.EndDate.Date; i= i.AddDays(1))
            {
                Attendance attendance= new Attendance { Employee_Id = vm.EmployeeId, Date = i, CheckInTime = vm.CheckInTime, CheckOutTime = vm.CheckOutTime, IsAbsent = vm.IsAbsent, IsOvertime = vm.IsOvertime, IsLTIncluded = vm.IsLTIncluded };
                if (!attendance.IsAbsent)
                {
                    if (attendance.IsOvertime)
                    {
                        attendance.Overtime = Pay.GetHourAsOvertime(attendance);
                        attendance.OvertimePay = Pay.GetPayAsOvertime(attendance);
                    }
                    else
                    {
                        attendance.Overtime = Pay.GetOvertime(attendance);
                        attendance.OvertimePay = Pay.GetDailyOvertimePay(attendance);
                        attendance.WorkHoursPay = Pay.GetBasicWorkHoursPay(attendance);
                    }
                }
                DateTime day = i.Date;
                //Only one instance of attendance of one day
                if(!db.Attendances.Any(x=>x.Employee_Id==vm.EmployeeId && (DbFunctions.TruncateTime(x.Date)==day) ) )
                db.Attendances.Add(attendance);
            }
            db.SaveChanges();
            return RedirectToAction("MarkAttendance", new { EmployeeId =vm.EmployeeId});
        }

        // GET: /Attendance/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = db.Attendances.Find(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", attendance.Employee_Id);
            return View(attendance);
        }

        // POST: /Attendance/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,IsAbsent,IsLTIncluded,IsOvertime,CheckInTime,CheckOutTime,WorkHoursPay,OvertimePay,Employee_Id")] Attendance attendance)
        {
            if (ModelState.IsValid)
            {
                if (!attendance.IsAbsent&&attendance.CheckOutTime.HasValue)
                {
                    AttendanceManagement.CheckOutOrEdit(attendance);
                    throw new StackOverflowException();
                }
                db.Entry(attendance).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.Employee_Id = new SelectList(db.Employees, "Id", "Name", attendance.Employee_Id);
            return View(attendance);
        }

        // GET: /Attendance/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = db.Attendances.Find(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            return View(attendance);
        }

        // POST: /Attendance/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Attendance attendance = db.Attendances.Find(id);
            db.Attendances.Remove(attendance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}






















////Attendances.Where(x => DbFunctions.TruncateTime(x.CheckInTime.Value.Date) == today).Include(x=>x.Employee).ToList();
////var employees = db.Employees.ToList();
////var emp_attend = (from a in db.Attendances
////                 join e in db.Employees on a.Employee_Id equals e.Id into ea
////                 from todayAttendance in ea.DefaultIfEmpty()
////                 where DbFunctions.TruncateTime(a.CheckInTime.Value.Date) == today
////                 select new
////                 {
////                   EmployeeId=  a.Employee_Id
////                 }).ToList();

////var emp_attend = db.Attendances.Where(x => (DbFunctions.TruncateTime(x.CheckInTime) == today)).join;
////var emp_attend = db.Employees.Include(x=>x.Attendances).Join( .ToList();
//string mystringjson = "";
//JavaScriptSerializer js = new JavaScriptSerializer();

////foreach (var item in emp_attend)
////{
////    mystringjson += "{'Name':'" + js.Serialize(item.Name)+"[";
////    foreach (var item2 in item.Attendances)
////    {
////        mystringjson += item2.CheckInTime;
////    }
////    mystringjson += "]";
////}

////foreach (var item in emp_attend)
////{
////    mystringjson += JsonConvert.SerializeObject(item.Name);
////    foreach (var item2 in item.Attendances)
////    {
////        mystringjson += JsonConvert.SerializeObject(item2.CheckInTime);
////    }

////}

////+"-" + js.Serialize(item.Attendances.FirstOrDefault(x => DbFunctions.TruncateTime(x.CheckInTime.Value.Date) == today)) + "****";
//return Json(JsonConvert.SerializeObject(emp_attend), JsonRequestBehavior.AllowGet);


 
            //var emp_attend2 = (from a in todayAtttendances
            //                  join e in db.Employees on a.Employee_Id equals e.Id into ea
            //                  from e in ea.DefaultIfEmpty()
            //                  select new
            //                  {
            //                      Id = e.Id,
            //                      Name = e.Name,
            //                      Designation = e.Designation,

            //                      CheckInTime = a.CheckInTime,
            //                      CheckOutTime = a.CheckOutTime,
            //                      Date = a.Date
            //                  }).ToList();
