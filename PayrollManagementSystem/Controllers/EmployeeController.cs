﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PayrollManagementSystem.Models;
using PayrollManagementSystem.ViewModels;

namespace PayrollManagementSystem.Controllers
{
    public class EmployeeController : BaseController
    {
        public ActionResult RefreshLeaves() {
            db.Employees.ToList().ForEach(x => {
                x.YearlyLeavesLeft = x.YearlyLeavesAllowed;
            });
            db.SaveChanges();
            return Json("success", JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowEmployeeList()
        {
            List<EmployeeListViewModel> l = new List<EmployeeListViewModel>();
            var list = db.Employees.ToList();
            foreach (var item in list)
                l.Add(new EmployeeListViewModel { Id = item.Id, Name = item.Name, FatherName = item.FatherName, Designation = item.Designation,JoiningDate=item.JoiningDate });

            var k = db.Employees.ToList();
            return PartialView("EmployeeList", l);
        }
        public ActionResult ShowEmployeeAttendence(int id) {
            var emp = db.Employees.Where(x =>x.Id == id).Select(x=>x).FirstOrDefault();
            return Json(emp,JsonRequestBehavior.AllowGet);
        }

        
        // GET: /Employee/
        public ActionResult Index()
        {
            return  View();
        }

        // GET: /Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: /Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,FatherName,Name,DOB,Address,Gender,PhoneNo,MobileNo,Email,JoiningDate,Designation,Picture,BasicSalary,isBoss,CNIC,YearlyLeavesAllowed,MonthlyLeavesAllowed,OvertimeRate")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.YearlyLeavesLeft = employee.YearlyLeavesAllowed;
                employee.MonthlyLeavesLeft = employee.MonthlyLeavesAllowed;
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: /Employee/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: /Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,FatherName,DOB,Address,Gender,PhoneNo,MobileNo,Email,JoiningDate,Designation,Picture,BasicSalary,isBoss,CNIC,YearlyLeavesAllowed,MonthlyLeavesAllowed,OvertimeRate,YearlyLeavesLeft,MonthlyLeavesLeft")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: /Employee/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: /Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
