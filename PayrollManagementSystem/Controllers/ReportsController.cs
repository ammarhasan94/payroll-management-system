﻿using Microsoft.Reporting.WebForms;
using PayrollManagementSystem.Models;
using PayrollManagementSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PayrollManagementSystem.Controllers
{
    public class ReportsController  : BaseController
    {
        //
        // GET: /Reports/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NoteCombination()
        {
            return View(new PayMonth { Month = DateTime.Now.AddMonths(-1) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NoteCombination(PayMonth payMonth)
        {
            if (ModelState.IsValid)
            {
                LocalReport lr = new LocalReport();
                string path = Path.Combine(Server.MapPath("~/Report"), "NoteCombination.rdlc");
                if (System.IO.File.Exists(path))
                    lr.ReportPath = path;
                else
                    return View("Index");

                #region Datatable "EmployeeSalary"
                DataTable d = new DataTable("EmployeeSalary");
                d.Columns.Add("Name", typeof(string));
                d.Columns.Add("Salary", typeof(double));
                d.Columns.Add("Id", typeof(int));
                d.Columns.Add("Designation", typeof(string));
                d.Columns.Add("FathersName", typeof(string));
                d.Columns.Add("PayMonth", typeof(DateTime));
                #endregion

                #region DataTable "NoteCombination"

                DataTable d2 = new DataTable("NoteCombination");
                d2.Columns.Add("Combination", typeof(string));
                #endregion

                int th, hun, ten, ones;
                th = hun = ten = ones = 0;
                var AllEmployees = db.Employees.ToList();
                foreach (var Employee in AllEmployees)
                {
                    var payroll = db.Payrolls.Where(x => x.Employee_Id == Employee.Id && x.PayDate.Month == payMonth.Month.Month).Select(x => x).FirstOrDefault();
                    if (payroll!= null)
                    {
                        d.Rows.Add(Employee.Name, payroll.NetPay.Value, Employee.Id, Employee.Designation, Employee.FatherName, payroll.PayDate);
                        var number = (int)Math.Round(payroll.NetPay.Value, 0);
                        th += number / 1000;
                        number = number % 1000;
                        hun += number / 100;
                        number = number % 100;
                        ten += number / 10;
                        number = number % 10;
                        ones += number ;
                    }
                }
                string noteCombination = th+" thousand's,"+hun+" hundred's,"+ten+" ten's and "+ones+" one's note";
                d2.Rows.Add(noteCombination);

                ReportDataSource rd1 = new ReportDataSource("EmployeeSalary", d);
                lr.DataSources.Add(rd1);
                ReportDataSource rd2 = new ReportDataSource("NoteCombination", d2);
                lr.DataSources.Add(rd2);

                #region Report
                string format = "PDF";
                string reportType = format;
                string mimeType;
                string encoding;
                string fileNameExtension;

                string deviceInfo =

                "<DeviceInfo>" +
                "  <OutputFormat>" + format + "</OutputFormat>" +
                "  <PageWidth>8.5in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>0.5in</MarginLeft>" +
                "  <MarginRight>0.5in</MarginRight>" +
                "  <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                renderedBytes = lr.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                #endregion
                return File(renderedBytes, mimeType);
            }

            return View(payMonth);
        }

        public ActionResult Payslip(int payrollId, string format)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "EmployeePayroll.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            Payroll p = db.Payrolls.Find(payrollId);
            List<Employee> cm = new List<Employee>();
            cm =db.Employees.Where(x=>x.Id==p.Employee_Id).Select(x=>x).ToList();
            ReportDataSource rd = new ReportDataSource("Employees", cm);
            lr.DataSources.Add(rd);

            List<Attendance> att = new List<Attendance>();
            att = db.Attendances.Where(x=>x.Date.Month==p.PayDate.Month && x.Employee_Id==p.Employee_Id).Select(x=>x).ToList();
            ReportDataSource rd1 = new ReportDataSource("Attendances", att);
            lr.DataSources.Add(rd1);

            List<Payroll> payroll = new List<Payroll>();
            payroll = db.Payrolls.Where(x=>x.PayDate.Month==p.PayDate.Month&&x.Employee_Id==p.Employee_Id).Select(x=>x).ToList();
            ReportDataSource rd2 = new ReportDataSource("Payrolls", payroll);
            lr.DataSources.Add(rd2);

            //DataTable d = new DataTable("DataTable1");
            //d.Columns.Add("LeavesThisMonth",typeof(int));
            //d.Columns.Add("RegHoursWorked", typeof(int));
            //int LeavesThisMonth = 0;
            //var leaves = db.Leaves1.Where(x => x.FromDate.Month == p.PayDate.Month && x.Employee_Id == p.Employee_Id).Select(x => x).ToList();
            //leaves.ForEach(l =>
            //{
            //    for (DateTime i = l.FromDate; i.Date <= l.ToDate.Date; i = i.AddDays(1))
            //    {
            //        if (i.Month == p.PayDate.Month)
            //            LeavesThisMonth++;
            //    }
            //});

            //double RegHoursWorked=0.0;
            //att.ForEach(x => {
            //    RegHoursWorked += (x.CheckOutTime.Value.Hour - x.CheckInTime.Value.Hour);
            //});
            //d.Rows.Add(LeavesThisMonth, RegHoursWorked);
            //ReportDataSource rd3=new ReportDataSource("DataSet1",d);
            //lr.DataSources.Add(rd3);

            //string type = "PDF";
            string reportType = format;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + format + "</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>0.5in</MarginLeft>" +
            "  <MarginRight>0.5in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }
	}
}